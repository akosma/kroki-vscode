# Kroki Diagram Previewer

This extension provides preview capabilities for any kind of diagram supported by [Kroki](https://kroki.io/).

![Demo](https://gitlab.com/akosma/kroki-vscode/raw/master/images/demo.gif)

The supported diagram types are the same supported by [Kroki](https://kroki.io/):

- [BlockDiag](https://github.com/blockdiag/blockdiag)
- [SeqDiag](https://github.com/blockdiag/seqdiag)
- [ActDiag](https://github.com/blockdiag/actdiag)
- [NwDiag](https://github.com/blockdiag/nwdiag)
- [C4 with PlantUML](https://github.com/RicardoNiepel/C4-PlantUML)
- [Ditaa](http://ditaa.sourceforge.net/)
- [Erd](https://github.com/BurntSushi/erd)
- [GraphViz](https://www.graphviz.org/)
- [Mermaid](https://github.com/knsv/mermaid)
- [Nomnoml](https://github.com/skanaar/nomnoml)
- [PlantUML](https://github.com/plantuml/plantuml)
- [Svgbob](https://github.com/ivanceras/svgbob)
- [UMlet](https://github.com/umlet/umlet)

The preview window always displays the SVG output for each diagram type.

## Install

Install the extension directly from [Visual Studio Code](https://code.visualstudio.com/) or [Codium](https://vscodium.com/), downloading it from the [marketplace](https://marketplace.visualstudio.com/items?itemName=akosma.kroki-vscode) or typing the command

`ext install akosma.kroki-vscode`

## Usage

While editing your diagram, launch the "Kroki: Preview…" command that corresponds to your diagram type.

## Configuration

This extension supports two configuration values:

1. `kroki.serverUrl`: (string) The URL of the Kroki instance used to generate diagrams.
   - Default value: `https://kroki.io`
2. `kroki.debug`: (boolean) Displays the URl of the server and the generated diagram in the preview window.
   - Default value: `false`

## Limitations

* This extension does not guess the diagram type from the editor or the file extension; Users have to choose the command that corresponds to the current diagram.
* There is no live reloading of the diagram when the source changes.

## Credits

This extension was inspired by the [mebrahtom/plantumlpreviewer](https://github.com/mebrahtom/plantumlpreviewer/) extension, and uses code provided in the [Kroki documentation](https://docs.kroki.io/kroki/setup/encode-diagram/).

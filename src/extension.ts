import * as vscode from 'vscode';

const pako = require('pako');

function getDiagramText() : string {
	let textContent = '';
	const currentEditor = vscode.window.activeTextEditor;
	if (currentEditor) {
		textContent = currentEditor.document.getText();
	}
	return textContent;
}

function getServerURL() : string {
	const config = vscode.workspace.getConfiguration();
	if (config) {
		const server = config.get('kroki.serverUrl') as string;
		if (server) {
			return server;
		}
	}
	return 'kroki.io';
}

function getDebug() : boolean {
	const config = vscode.workspace.getConfiguration();
	if (config) {
		const debug = config.get("kroki.debug") as boolean | undefined;
		if (debug) {
			return debug;
		}
	}
	return false;
}

function getDiagramURL(data: string, kind: string, serverURL : string) : string {
	// Borrowed from
	// https://docs.kroki.io/kroki/setup/encode-diagram/
	const buffer = Buffer.from(data, 'utf8');
	const compressed = pako.deflate(buffer, { level: 9 });
	const diagram = Buffer.from(compressed).toString('base64').replace(/\+/g, '-').replace(/\//g, '_');

	// Using SVG, which is the only output format supported by all diagram kinds
	const url = `https://${serverURL}/${kind}/svg/` + diagram;
	return url;
}

function createHTML(url: string, serverURL: string, debug: boolean) : string {
	let debugInformation = '';
	if (debug) {
		debugInformation = `<p>Kroki: ${serverURL}</p>
		<p>URL: <a href="${url}">${url}</a></p>`
	}
	return `<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Kroki Preview</title>
</head>
<body style="background-color: white; color: black;">
<p><img src=${url} alt="Kroki diagram" width="100%" /></p>
${debugInformation}
</body>
</html>`;
}

function diagramKindName(kind: string) : string {
	const mapping : { [name: string] : string } = {
		"blockdiag": "BlockDiag",
		"seqdiag": "SeqDiag",
		"actdiag": "ActDiag",
		"nwdiag": "NwDiag",
		"c4plantuml": "C4 with PlantUML",
		"ditaa": "Ditaa",
		"erd": "Erd",
		"graphviz": "GraphViz",
		"mermaid": "Mermaid",
		"nomnoml": "Nomnoml",
		"plantuml": "PlantUML",
		"svgbob": "Svgbob",
		"umlet": "UMlet"
	};
	const name = mapping[kind];
	if (name) {
		return name;
	}
	return "";
}

function createPanel(kind: string, name: string) : vscode.WebviewPanel {
	return vscode.window.createWebviewPanel(
		`kroki.${kind}`,
		`Kroki ${name} Preview`,
		{
			viewColumn: vscode.ViewColumn.Beside,
			preserveFocus: false
		},
		{
			enableScripts: true
		},
	);
}

export function activate(context: vscode.ExtensionContext) : void {
	["blockdiag", "seqdiag", "actdiag", "nwdiag", "c4plantuml", "ditaa",
		"erd", "graphviz", "mermaid", "nomnoml", "plantuml", "svgbob", "umlet"].map((kind) => {
			const command = vscode.commands.registerCommand(`extension.kroki.${kind}`, () => {
				const name = diagramKindName(kind);
				const panel = createPanel(kind, name);
				const serverURL = getServerURL();
				const debug = getDebug();
				const diagramText = getDiagramText();
				const url = getDiagramURL(diagramText, kind, serverURL);
				panel.webview.html = createHTML(url, serverURL, debug);
			});

			context.subscriptions.push(command);
		})
}

export function deactivate() : void { }
